\documentclass[10pt,a4paper,titlepage]{article}
\usepackage[utf8x]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[official]{eurosym}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage[table]{xcolor}
\usepackage{geometry}
\geometry{top=2.54cm, left=2.54cm, right=2.54cm, bottom=2.54cm}
\setlength{\parskip}{0.5em}
\usepackage{hyperref}
\usepackage{fancyref}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{wrapfig}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}

\expandafter\def\expandafter\normalsize\expandafter{%
    \normalsize
    \setlength\abovedisplayshortskip{10pt}
    \setlength\belowdisplayshortskip{10pt}
}

\title{ \textbf{ \Huge{Práctica 1: \\Utilizando Redes de Neuronas para clasificación y predicción}}}
\author{
		\begin{tabular}{lr}
			\multicolumn{1}{l}{Grupo 83} \\ \cline{1-1} \\
			Daniel Alejandro Rodríguez López & 100316890 \\
			Adrián Borja Pimentel & 100315536 \\
			Mario Montes González & 100318104 \\
			Axel Blanco Cerro & 100315991 \\
		\end{tabular}
}

\begin{document}
\maketitle
\tableofcontents
\newpage

\section{Introducción}
	Durante la realización de esta práctica se van a resolver dos problemas distintos utilizando Redes de Neuronas Artificiales (RNA). El primero de ellos consiste en, mediante clasificación, determinar lo buenos que son los coches de segunda mano que adquirirá una empresa con el fin de venderlos posteriormente. En el segundo problema se utilizará predicción para estimar la cantidad de dinero que se extraerá en un futuro de distintos cajeros automáticos. Para ambos problemas ha sido proporcionado un registro de datos con los que generar las estructuras.

	A lo largo del documento dedicaremos una sección al desarrollo de cada problema, donde se explicarán las distintas decisiones que hemos tomado para generar las RNA, se evaluarán los resultados obtenidos, comparándolos con los datos reales que se han intentado predecir, y se hará una conclusión de la solución obtenida para el problema. Por último se incluirá una sección adicional donde se presentarán conclusiones y reflexiones generales que han surgido durante la realización de la práctica.


\section{Clasificación con RNA}

	En este apartado se pide realizar un clasificador con RNA. Para ello se proporciona el archivo \textit{CarData.arff}, el cual contiene poco más de 1700 ejemplos clasificados previamente.

	\subsection{Preparación de los datos}

	Las distintas características que poseen los datos son todas de tipo nominal, por lo cual para poder ser utilizadas por RNA deben transformarse a valores numéricos, este proceso lo realiza automáticamente Weka cuando ejecutamos el Perceptrón Multicapa para crear un clasificador.
	
	Por lo tanto, de los filtros indicados:
	\begin{itemize}
		\item {Discretizar}: No tiene mucho sentido cambiar el valor numérico por un valor nominal, para que posteriormente la red lo vuelva a transformar a numérico. Podría tener sentido cuando el rango de números es muy grande, pero los máximos valores distintos que podemos tener en un atributo son hasta cuatro, por lo que no tiene sentido convertir valores 0,1,2 y 3 en rangos. Se perdería información o los rangos generados acabarían siendo cuatro rangos entre [0,1),[1,2),[2,3) y [3,$\infty$), por lo que todo se quedaría igual. Sí tiene sentido discretizar, sin embargo, cuando se tienen valores nominales, para poder tratarlos de forma numérica (de otra forma no pueden ser procesados por la red).
		\item {Normalizar}: Es conveniente, siempre dependiendo con los datos que trabajemos, normalizar los datos. Sin embargo, dado que los valores de entrada son nominales, habría que transformarlos a número previamente, lo cual realiza Weka automáticamente en el caso del perceptrón multicapa. El hecho de normalizarlos, en el caso concreto de las redes de neuronas, es especialmente importante dado que los sistemas MLP se comportan mucho mejor con valores entre 0 y 1 (normalizados) que con valores que abarquen todo el espectro de los reales.
	\end{itemize}

	Un filtro que sería conveniente aplicar es \textbf{Aleatorizar} los datos. Esto es siempre recomendable para evitar que los patrones se encuentren seguidos en el archivo de datos, para evitar que aprenda un patrón y cuando tiene que aprender otro, se `olvida' del que acaba de aprender.

	\subsection{Resultados del Perceptrón Multicapa}

	A continuación podemos ver dos tablas con los datos relativos a las redes de neuronas calculadas, con sus parámetros. La primera tabla hace referencia a los resultados sin aplicar la aleatorización y la segunda se ha generado por medio de instancias aleatorizados (tiene especial interés la red generada con test por medio de \textit{Percentant-split}).

	Los resultados obtenidos sin aleatorizar los datos son los siguientes:
	\begin{center}
	\begin{tabular}{l | l | l | l}
	Método de test		 	& HiddenLayer & TrainingTime 	& Error relativo \\ \hline
	Cross-Validation(10) 	& Automatico 	& 500 			& 2.69 \\
	Cross-Validation(10) 	& 7 			& 500 			& 3.59 \\
	Cross-Validation(10) 	& 7				& 200			& 5.59 \\
	Percentant-split(70\%) 	& 7				& 200			& 6.17 \\ \hline
	\end{tabular}
	\end{center}

	Mientras que aleatorizando, los resultandos son:
	\begin{center}
	\begin{tabular}{l | l | l | l}
	Método de test		 	& HiddenLayer & TrainingTime 	& Error relativo \\ \hline
	Cross-Validation(10) 	& Automatico 	& 500 			& 3.38 \\
	Cross-Validation(10) 	& 7 			& 500 			& 5.03 \\
	Cross-Validation(10) 	& 7				& 200			& 7.56 \\
	Percentant-split(70\%) 	& 7				& 200			& 7.00 \\ \hline
	\end{tabular}
	\end{center}

	Como se puede apreciar el error aumenta ligeramente, pero a diferencia del primer caso, cuando se pasa a utilizar división porcentual el error se mantiene, llegando a bajar en 0.5. Esto nos indica que es bueno generalizando ya que al utilizar instancias que no se usaron para entrenar la red, es capaz de acertar hasta un 93\% de las mismas. Además, observando la matriz de confusión se puede observar que el número de coches inaceptables mal clasificados son sólo 2 de 353, por lo que es un porcentaje muy pequeño.
	
	Además, se debe tener en cuenta que ese aumento del error relativo en caso de aleatorizar las instancias está posiblemente derivado de un ligero sobre ajuste del MLP al set de ejemplos en el caso en que no se aleatoriza, por lo que sería recomendable hacer uso de la red generada por ejemplos aleatorizados. Este problema se puede corregir de forma sencilla usando otras herramientas para generar la red de neuronas, como programas específicos o incluso implementaciones manuales con librerías específicas en las que se puedan controlar mejor los parámetros de caracterización de la red, como el número de capas ocultas y los nodos de cada una, así como la distribución inicial de pesos.
	
	\subsection{Resultados con otras técnicas de clasificación}

	Para poder comparar la clasificación por medio de redes de neuronas se han realizado pruebas con otras técnicas de clasificación típicas, como árboles de decisión. Si bien hemos realizado diferentes formatos de árboles usando, principalmente, J48 e ID3, por claridad y limpieza, así como por la similitud entre ellos hemos incluido una tabla con los datos únicamente de un subconjunto de los J48.

	En este caso concreto tenemos árboles de decisión generados por medio de J48 con mínimo 5 elementos en cada nodo, se obtienen los siguientes resultados:
	\begin{center}
	\begin{tabular}{l | l }
	Método de test		 	& Error relativo \\ \hline
	Cross-Validation(10) 	& 33.39 \\
	Percentant-split(70\%) 	& 34.17 \\ \hline
	\end{tabular}
	\end{center}

	Como se puede ver, el error relativo se dispara, además el número de coches inaceptables mal clasificados es muchísimo mayor que usando las RNA. Aunque el error relativo se dispare, las instancias bien clasificadas son casi del 90\% (87 y 88 respectivamente).

	Si se siguen realizando pruebas y cambiando valores de J48 o la división de los datos de entrada, apenas sufren variaciones los resultados obtenidos, por lo que parece ser que J48, al igual que sería ID3, no serían malos clasificadores para esta situación.

	De igual forma, las redes de neuronas no han mostrado un mal comportamiento y el porcentaje de acierto que obtienen es mayor que en J48, por lo que en el caso de elegir una de las dos técnicas, usar redes de neuronas daría mejor resultado. Además, se debe tener en cuenta que la red de neuronas es capaz de discriminar de forma más precisa ejemplos extremos.

\section{Predicción con RNA}
	El segundo problema requiere predecir el dinero que se extraerá en el futuro de un cajero automático. Para ello contamos con 791 datos para tres cajeros distintos, los primeros 781 se utilizarán para entrenar la red y los 10 últimos para evaluarla.

	\subsection{Proceso de entrenamiento}
		Lo primero que se debe hacer es escoger los k valores que van a formar la serie temporal que usaremos en la resolución del problema. Partiendo de los datos recogidos en el fichero \textit{DatosCajero.xls} proporcionado, hemos probado con series de 7 y 14 días. Los ficheros generados para el entrenamiento se encuentran en \texttt{parte2/training}.
		
		Para escoger la red que mejor funcione, hemos generado las siguientes modificando el número de datos de entrada y el número de neuronas en la capa oculta:
		
		\begin{itemize}
			\item 15 entradas (14 días + predicción), 4 nodos ocultos
			\item 15 entradas (14 días + predicción), 7 nodos ocultos (automático por Weka)
			\item 8 entradas (7 días + predicción), 6 nodos ocultos
			\item 8 entradas (7 días + predicción), 4 nodos ocultos (automático por Weka)
		\end{itemize}
		
		Estas redes se han entrenado con los datos del cajero 1. Los modelos obtenidos se adjuntan en el directorio \texttt{parte2/models}. Los resultados obtenidos mediante cross validation de 10 folds son los siguientes:
		
		\begin{center}
		\begin{tabular}{l | l | l }
		Entradas & Nodos ocultos & Error relativo \\
		\hline
		15 & 4 & 79,31 \% \\
		15 & 7 & 80,53 \% \\
		8 & 6 & 76,03 \% \\
		8 & 4 & 77,12 \% \\
		\hline
		\end{tabular}
		\end{center}
			
		Hemos determinado escoger la tercera red (8 entradas, 6 nodos ocultos) ya que es la que menor error proporciona.

	\subsection{Procesos de predicción}

		\begin{center}
			\includegraphics[scale=0.6]{../parte2/Grafica_Prediccion}
		\end{center}

		Al ver los resultados obtenidos y compararlos con los reales, podemos ver que nuestra predicción no es del todo buena, creemos que un posible causante de esto sean las instancias pertenecientes a los días en los que los cajeros no se encontraban operativos. Consideramos la posibilidad de retirar estás instancias, pero dada la representación real de la serie, es decir, las extracciones planteadas en semanas, consideramos que mantener la coherencia era preferible y que, en cierto modo, no poder realizar una extracción un día concreto, influiría en los días posteriores, por lo que hemos preferido dejarlo.
		
		Además, tal y cómo está planteada la predicción el valor de un día afecta al siguiente por lo que el error obtenido en la predicción se irá acumulando en las siguientes.

	\subsection{Comparación con línea de tendencia}
		Se ha comparado los valores de los últimos 10 días con las predicciones de la línea de tendencia, se ha utilizado el tipo polinómico, ya que era el que mejor se ajustaba y más se asemejaría a la linea que describen los datos.
		\subsubsection{Cajero 1}
		\begin{center}
			\includegraphics[scale=0.6]{../parte2/Grafica_lt1}
		\end{center}

		Aun que no lo parezca la linea de tendencia es de tipo polinómica, sin embargo la predicción es realmente mala, prácticamente media, por lo que consideramos que usarla para predecir sería un mala decisión la mayoría de las veces.

		\subsubsection{Cajero 2}
		\begin{center}
			\includegraphics[scale=0.6]{../parte2/Grafica_lt2}
		\end{center}

		El comportamiento de la línea de tendencia en el segundo cajero es igual de catastrófico que en el primero, con el añadido de que en este cajero los datos reales son más variables, por lo que la predicción tiene aún error aún mayor.

		\subsubsection{Cajero 3}
		\begin{center}
			\includegraphics[scale=0.6]{../parte2/Grafica_lt3}
		\end{center}

		Las predicciones de este cajero son realmente malas.

		\subsubsection{Conclusiones generales de la línea de tendencia}
			Viendo los resultados nos planteamos que quizá no hayamos generado las líneas de tendencia correctamente, ya que utilizarlas como herramienta de predicción seria una idea horrible ni siquiera preferible a un algoritmo aleatorio en el tercer cajero.


\section{Conclusiones y reflexiones sobre la práctica}
	Nos ha parecido interesante la realización de la práctica al ver tan claramente casos prácticos donde las empresas se valen de técnicas de IA para diseñar sus estrategias y organizar eficientemente sus recursos. Viendo esto hablamos sobre varias áreas donde seguramente aplican técnicas similares o, si no lo hacen, deberían, por ejemplo a la hora de reponer productos perecederos en los supermercados, calcular la afluencia de gente a un evento en función de la actividad en las redes, descubrir cuales son los mejores temas que reproducir en una discoteca o tocar en un concierto...

	Otra reflexión que hemos debatido durante la práctica es la utilidad de los diversos métodos para predecir, viendo lo mal que han funcionado las líneas de tendencia, ¿Es posible que haya técnicas de predicción siempre preferibles a otras?, hemos llegado a la conclusión de que no, ya que si fuera así no se enseñarían ni utilizarían el resto de técnicas, por lo que saber en que problemas se debe usar cada una debería ser una capacidad deseable y de gran importancia en nuestra formación.

\end{document}
