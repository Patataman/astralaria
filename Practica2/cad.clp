; Entradas
(deftemplate dist-longitudinal
    0.0 100.0 metros
    (
    (colision (0.0 1) (0.05 0))
    (muy-cerca (0.025 0) (0.05 1) (1.0 1) (2 0))
    (cerca (1.5 0) (2 1) (10 1) (15 0))
    (normal (12.5 0) (15 1) (25 1) (30 0))
    (lejos (27.5 0) (30 1) (50 1) (60 0))
    (muy-lejos (55 0) (60 1))
))

(deftemplate dist-lateral
    0.0 3.0 metros
    (
    (nula (0.0 1) (0.1 0))
    (muy-cerca (0.05 0) (0.1 1) (0.2 1) (0.3 0))
    (cerca (0.25 0) (0.3 1) (0.5 1) (1 0))
    (normal (0.5 0) (1 1) (1.5 1) (2 0))
    (lejos (1.5 0) (2.0 1))
))


; Salidas
(deftemplate valor-pedal 
    0 100 valor-ECU
    ((nulo (0 1) (0 0))
    (minimo (0 0) (1 1) (5  1) (10 0))
    (moderado (5 0) (10 1) (20 1) (30 0)) 
    (fuerte (20 0) (30 1) (50 1) (60 0))
    (afondo (50 0) (60 1))
))

(deftemplate angulo-giro 
    -450 450 grados
    ((izq-completo (-450 1) (-445 0)) 
    (izq-mucho (-450 0) (-445 1) (-360 1) (-355 0))
    (izq-moderado (-360 0) (-355 1) (-180 1) (-170 0))
    (izq-medio (-180 0) (-170 1) (-90 1) (-85 0))
    (izq-poco (-90 0) (-85 1) (-15 1) (-10 0))
    (izq-muy-poco (-15 0) (-10 1) (-0.5 0))
    (nulo (-0.5 0) (0 1) (0.5 0))
    (dcha-muy-poco  (0.5 0) (10 1) (15 0))
    (dcha-poco (10 0) (15 1) (85 1) (90 0) )
    (dcha-medio (85 0)(90 1) (170 1) (180 0) )
    (dcha-moderado (170 0) (180 1) (355 1) (360 0))
    (dcha-mucho (355 0) (360 1) (445 1) (450 0))
    (dcha-completo (445 0) (450 0))
))


; Slots
(deftemplate siguiente-nivel-pedal
    (slot idnivel (type SYMBOL))
    (slot valor (type FUZZY-VALUE valor-pedal))
    (slot siguiente (type FUZZY-VALUE valor-pedal))
)

(deftemplate siguiente-giro-volante
    (slot idnivel (type SYMBOL))
    (slot valor (type FUZZY-VALUE angulo-giro))
    (slot siguiente (type FUZZY-VALUE angulo-giro))
)

(deftemplate instante-analisis
    (slot t (type INTEGER))
    (slot sensor-radar (type FUZZY-VALUE dist-longitudinal))
    (slot KLA-dist-izq (type FUZZY-VALUE dist-lateral))
    (slot KLA-dist-dcha (type FUZZY-VALUE dist-lateral))
    (slot velocidad-maxima (type INTEGER))
)

(deftemplate controles
    (slot id (type SYMBOL))
    (slot pedal-acelerador (type FUZZY-VALUE valor-pedal))
    (slot pedal-freno (type FUZZY-VALUE valor-pedal))
    (slot giro-volante (type FUZZY-VALUE angulo-giro))
    (slot velocidad (type INTEGER))
)


; Datos de prueba
(deffacts lista-instantes
    (instante-analisis (t 1)
        (velocidad-maxima 100)
        (sensor-radar normal)
        (KLA-dist-izq cerca)
        (KLA-dist-dcha lejos)
    )
    (instante-analisis (t 2)
        (velocidad-maxima 70)
        (sensor-radar cerca)
        (KLA-dist-izq muy-cerca)
        (KLA-dist-dcha lejos)
    )
    (instante-analisis (t 3)
        (velocidad-maxima 70)
        (sensor-radar cerca)
        (KLA-dist-izq muy-cerca)
        (KLA-dist-dcha lejos)
    )
    (instante-analisis (t 4)
        (velocidad-maxima 70)
        (sensor-radar cerca)
        (KLA-dist-izq normal)
        (KLA-dist-dcha normal)
    )
    (instante-analisis (t 5)
        (velocidad-maxima 70)
        (sensor-radar cerca)
        (KLA-dist-izq cerca)
        (KLA-dist-dcha lejos)
    )
)

; Inicialización
(deffacts secuencia-niveles-pedal
    (siguiente-nivel-pedal (idnivel n1) (valor nulo) (siguiente minimo))
    (siguiente-nivel-pedal (idnivel n2) (valor minimo) (siguiente moderado))
    (siguiente-nivel-pedal (idnivel n3) (valor moderado) (siguiente fuerte))
    (siguiente-nivel-pedal (idnivel n4) (valor fuerte) (siguiente afondo))
)

;El sentido del volante va de drch a izq. Es decir, en la izq está el máximo (por lo que para girar a la drch hay que coger el valor anterior)
; y en la drch el mínimo, por lo que para ir a la izq hay que coger el siguiente
(deffacts secuencia-niveles-volante
    (siguiente-giro-volante (idnivel g1) (valor izq-mucho) (siguiente izq-completo))
    (siguiente-giro-volante (idnivel g2) (valor izq-moderado) (siguiente izq-mucho))
    (siguiente-giro-volante (idnivel g3) (valor izq-medio) (siguiente izq-moderado))
    (siguiente-giro-volante (idnivel g4) (valor izq-poco) (siguiente izq-medio))
    (siguiente-giro-volante (idnivel g5) (valor izq-muy-poco) (siguiente izq-poco))
    (siguiente-giro-volante (idnivel g6) (valor nulo) (siguiente izq-muy-poco))
    (siguiente-giro-volante (idnivel g7) (valor dcha-muy-poco) (siguiente nulo))
    (siguiente-giro-volante (idnivel g7) (valor dcha-poco) (siguiente dcha-muy-poco))
    (siguiente-giro-volante (idnivel g8) (valor dcha-medio) (siguiente dcha-poco))
    (siguiente-giro-volante (idnivel g9) (valor dcha-moderado) (siguiente dcha-medio))
    (siguiente-giro-volante (idnivel g10) (valor dcha-mucho) (siguiente dcha-moderado))
    (siguiente-giro-volante (idnivel g11) (valor dcha-moderado) (siguiente dcha-mucho))
    (siguiente-giro-volante (idnivel g12) (valor dcha-completo) (siguiente dcha-moderado))

)

; NOTA: Solo existirá una instancia de este hecho en cada instante
(deffacts valores-controles
    (controles (id idCTRL) (pedal-acelerador minimo) (pedal-freno nulo)
    (giro-volante izq-moderado) (velocidad 80))
)

;Nos acercamos a la izquierda -> Hay que ir a la drch
(defrule CAD-izq-1
    ;(declare (salience 20))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-izq cerca))
    (instante-analisis (t ?t2) (KLA-dist-izq muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente
    ?gv1))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos acercamos a la izquierda -> Hay que ir a la drch
(defrule CAD-izq-2
    ;(declare (salience 15))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-izq normal))
    (instante-analisis (t ?t2) (KLA-dist-izq cerca))
    (test (eq (- ?t2 ?t1) 1))
        ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente
    ?gv1))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Se está muy cerca de la izq -> Hay que ir a la drch
(defrule CAD-izq-3
    ;(declare (salience 25))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-izq muy-cerca))
    (instante-analisis (t ?t2) (KLA-dist-izq muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente
    ?gv1))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos desviamos a la drch, hay que cambiar a la izquierda
(defrule CAD-izq-4
    ;(declare (salience 10))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-izq normal))
    (instante-analisis (t ?t2) (KLA-dist-izq lejos))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la drch y queremos cambiar a la izq
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)

;Nos acercamos a la drch -> Hay que ir a la izq
(defrule CAD-dcha-1
    ;(declare (salience 20))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-dcha cerca))
    (instante-analisis (t ?t2) (KLA-dist-dcha muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el siguiente al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos acercamos a la drch -> Hay que ir a la izq
(defrule CAD-dcha-2
    ;(declare (salience 15))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-dcha normal))
    (instante-analisis (t ?t2) (KLA-dist-dcha cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el siguiente al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Se está muy cerca de la drch -> Hay que ir a la izq
(defrule CAD-dcha-3
    ;(declare (salience 25))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-dcha muy-cerca))
    (instante-analisis (t ?t2) (KLA-dist-dcha muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el siguiente al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos desviamos a la izquierda, hay que girar a la drch
(defrule CAD-dcha-4
    ;(declare (salience 10))
    ?t1Inst <- (instante-analisis (t ?t1) (KLA-dist-dcha normal))
    (instante-analisis (t ?t2) (KLA-dist-dcha lejos))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente
    ?gv1))
=>
    (retract ?c)
    (retract ?t1Inst)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
