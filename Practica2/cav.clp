; Entradas
(deftemplate dist-longitudinal 0.0 100.0 metros
  (
  (colision  (0.0 1) (0.05 0))
  (muy-cerca (0.05 0) (0.1 1) (1.0 1) (2 0))
  (cerca (1.5 0) (2 1) (10 1) (15 0))
  (normal (12.5 0) (15 1) (25 1) (30 0))
  (lejos (27.5 0) (30 1) (50 1) (60 0))
  (muy-lejos (55 0) (60 1))
))


(deftemplate dist-lateral 0.0 3.0 metros
  (
  (nula (0.0 1) (0.1 0))
  (muy-cerca (0.05 0) (0.1 1) (0.2 1) (0.3 0))
  (cerca (0.25 0) (0.3 1) (0.5 1) (1 0))
  (normal (0.5 0)  (1 1)  (1.5 1) (2 0))
  (lejos (1.5 0) (2.0 1))
))

(deftemplate valor-pedal 0 100 valor-ECU
(
  (nulo (0 0) (0 0))
  (minimo (0 0) (1 1) (5  1) (10 0))
  (moderado (5 0) (10 1) (20 1) (30 0))
  (fuerte (20 0) (30 1) (50 1) (60 0))
  (afondo (50 0) (60 1))
))

(deftemplate angulo-giro -450 450 grados
(
  (izq-completo (-450 1) (-445 0))
  (izq-mucho (-450 0) (-445 1) (-360 1) (-355 0))
  (izq-moderado (-360 0) (-355 1) (-180 1) (-170 0))
  (izq-medio (-180 0) (-170 1) (-90 1) (-85 0))
  (izq-poco (-90 0) (-85 1) (-15 1) (-10 0))
  (izq-muy-poco (-15 0) (-10 1) (0 0))
  (nulo (0 0) (0 0))
  (dcha-muy-poco  (0 0) (10 1) (15 0))
  (dcha-poco (10 0) (15 1) (85 1) (90 0) )
  (dcha-medio (85 0)(90 1) (170 1) (180 0) )
  (dcha-moderado (170 0) (180 1) (355 1) (360 0))
  (dcha-mucho (355 0) (360 1) (445 1) (450 0))
  (dcha-completo (445 0) (450 0))
))
; Slots
(deftemplate siguiente-nivel-pedal
  (slot idnivel(type SYMBOL))
  (slot valor (type FUZZY-VALUE valor-pedal))
  (slot siguiente (type FUZZY-VALUE valor-pedal))
)

(deftemplate instante-analisis
  (slot t (type INTEGER))
  (slot sensor-radar (type FUZZY-VALUE dist-longitudinal))
  (slot KLA-dist-izq (type FUZZY-VALUE dist-lateral))
  (slot KLA-dist-dcha (type FUZZY-VALUE dist-lateral))
  (slot velocidad-maxima (type INTEGER))
)


(deftemplate controles
  (slot id (type SYMBOL))
  (slot pedal-acelerador (type FUZZY-VALUE valor-pedal))
  (slot pedal-freno (type FUZZY-VALUE valor-pedal))
  (slot giro-volante (type FUZZY-VALUE angulo-giro))
  (slot velocidad (type INTEGER))
)

; Inicializaci�n
(deffacts secuencia-niveles-pedal
  (siguiente-nivel-pedal (idnivel n1) (valor nulo) (siguiente minimo))
  (siguiente-nivel-pedal (idnivel n2) (valor minimo) (siguiente moderado))
  (siguiente-nivel-pedal (idnivel n3) (valor moderado) (siguiente fuerte))
  (siguiente-nivel-pedal (idnivel n4) (valor fuerte) (siguiente afondo))
)

; Reglas
; Disminuye el nivel del acelerador en caso de sobrepasar la velocidad m�xima
(defrule CAV-deceleracion
  (instante-analisis (t ?t2)(velocidad-maxima ?vm))
  ?c <- (controles (id ?id)(pedal-acelerador ?pa)(velocidad ?v)(giro-volante ?gv))
  (test (= 0 (mod ?t2 2)))
  (test (> ?v ?vm))
  (siguiente-nivel-pedal (idnivel ?idnivel)(valor ?nuevo-nivel)(siguiente ?pa))
  =>
  (retract ?c)
  (printout t "Acci�n - Soltar acelerador a " ?nuevo-nivel crlf)
  (plot-fuzzy-value t "*" nil nil ?nuevo-nivel)
  (assert (controles (id ?id)(pedal-acelerador ?nuevo-nivel)(pedal-freno nulo)(velocidad ?v )(giro-volante ?gv)))
)

;Caso en el que la velocidad actual es menor que la m�nima.
;Para acelerar OBLIGATORIAMENTE la distancia con el coche de delante debe ser lejos (distancia de seguridad)
(defrule CAV-aceleracion
  (instante-analisis (t ?t2)(sensor-radar lejos)(velocidad-maxima ?vm))
  ?c <- (controles (id ?id)(pedal-acelerador ?pa)(velocidad ?v)(giro-volante ?gv))
  ;Para acelerar la distancia debe ser lejos
  (test (= 0 (mod ?t2 2)))
  (test (< ?v ?vm))
  (siguiente-nivel-pedal (idnivel ?idnivel)(valor ?pa)(siguiente ?nuevo-nivel))
  =>
  (retract ?c)
  (printout t "Acci�n - Soltar acelerador a " ?nuevo-nivel crlf)
  (plot-fuzzy-value t "*" nil nil ?nuevo-nivel)
  (assert (controles (id ?id)(pedal-acelerador ?nuevo-nivel)(pedal-freno nulo)(velocidad ?v )(giro-volante ?gv)))
)


; Datos de prueba // Reducir a nulo
(deffacts lista-instantes
  (instante-analisis (t 1)
    (velocidad-maxima 100)
    (sensor-radar normal)
    (KLA-dist-izq cerca)
    (KLA-dist-dcha lejos))
  (instante-analisis (t 2)
    (velocidad-maxima 70)
    (sensor-radar cerca)
    (KLA-dist-izq cerca)
    (KLA-dist-dcha lejos))
)

(deffacts valores-controles
  (controles (id idCTRL) (pedal-acelerador minimo) (pedal-freno nulo) (giro-volante nulo) (velocidad 80))
)

; Datos de prueba // Nada
(deffacts lista-instantes
  (instante-analisis (t 1)
    (velocidad-maxima 100)
    (sensor-radar normal)
    (KLA-dist-izq cerca)
    (KLA-dist-dcha lejos))
  (instante-analisis (t 2)
    (velocidad-maxima 90)
    (sensor-radar cerca)
    (KLA-dist-izq cerca)
    (KLA-dist-dcha lejos))
)

(deffacts valores-controles
  (controles (id idCTRL) (pedal-acelerador minimo) (pedal-freno nulo) (giro-volante nulo) (velocidad 80))
)

; Datos de prueba // Acelerar
(deffacts lista-instantes
  (instante-analisis (t 1)
    (velocidad-maxima 100)
    (sensor-radar normal)
    (KLA-dist-izq cerca)
    (KLA-dist-dcha lejos))
  (instante-analisis (t 2)
    (velocidad-maxima 100)
    (sensor-radar lejos)
    (KLA-dist-izq cerca)
    (KLA-dist-dcha lejos))
)

(deffacts valores-controles
  (controles (id idCTRL) (pedal-acelerador moderado) (pedal-freno nulo) (giro-volante nulo) (velocidad 70))
)
