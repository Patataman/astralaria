;==========================================================
; Definición de plantillas predefinidas
;==========================================================

(deftemplate dist-longitudinal 0.0 100.0 metros
    (
    (colision  (0.0 1) (0.05 0))
    (muy-cerca (0.05 0) (0.1 1) (1.0 1) (2 0))
    (cerca (1.5 0) (2 1) (10 1) (15 0))
    (normal (12.5 0) (15 1) (25 1) (30 0))
    (lejos (27.5 0) (30 1) (50 1) (60 0))
    (muy-lejos (55 0) (60 1))
))

(deftemplate dist-lateral 0.0 3.0 metros
    (
    (nula (0.0 1) (0.1 0))
    (muy-cerca (0.05 0) (0.1 1) (0.2 1) (0.3 0))
    (cerca (0.25 0) (0.3 1) (0.5 1) (1 0))
    (normal (0.5 0)  (1 1)  (1.5 1) (2 0))
    (lejos (1.5 0) (2.0 1))
))

(deftemplate angulo-giro -450 450 grados
(
    (izq-completo (-450 1) (-445 0)) 
    (izq-mucho (-450 0) (-445 1) (-360 1) (-355 0))
    (izq-moderado (-360 0) (-355 1) (-180 1) (-170 0))
    (izq-medio (-180 0) (-170 1) (-90 1) (-85 0))
    (izq-poco (-90 0) (-85 1) (-15 1) (-10 0))
    (izq-muy-poco (-15 0) (-10 1) (0 0))
    (nulo (0 0) (0 0))
    (dcha-muy-poco  (0 0) (10 1) (15 0))
    (dcha-poco (10 0) (15 1) (85 1) (90 0) )
    (dcha-medio (85 0)(90 1) (170 1) (180 0) )
    (dcha-moderado (170 0) (180 1) (355 1) (360 0))
    (dcha-mucho (355 0) (360 1) (445 1) (450 0))
    (dcha-completo (445 0) (450 0)) 
))

(deftemplate valor-pedal 0 100 valor-ECU
(
    (nulo (0 0) (0 0))
    (minimo (0 0) (1 1) (5  1) (10 0))
    (moderado (5 0) (10 1) (20 1) (30 0)) 
    (fuerte (20 0) (30 1) (50 1) (60 0))
    (afondo (50 0) (60 1))
))

(deftemplate siguiente-nivel-pedal
    (slot idnivel(type SYMBOL))
    (slot valor (type FUZZY-VALUE valor-pedal))
    (slot siguiente (type FUZZY-VALUE valor-pedal))
)

(deftemplate instante-analisis
    (slot t (type INTEGER))
    (slot sensor-radar (type FUZZY-VALUE dist-longitudinal))
    (slot KLA-dist-izq (type FUZZY-VALUE dist-lateral))
    (slot KLA-dist-dcha (type FUZZY-VALUE dist-lateral))
    (slot velocidad-maxima (type INTEGER))
)

(deftemplate controles
    (slot id (type SYMBOL))
    (slot pedal-acelerador (type FUZZY-VALUE valor-pedal))
    (slot pedal-freno (type FUZZY-VALUE valor-pedal))
    (slot giro-volante (type FUZZY-VALUE angulo-giro))
    (slot velocidad (type INTEGER))
)

;==========================================================
; FIN plantillas predefinidas
;==========================================================

;==========================================================
;---------------------_PLANTILLAS_CAD_---------------------
;==========================================================

(deftemplate siguiente-giro-volante
    (slot idnivel (type SYMBOL))
    (slot valor (type FUZZY-VALUE angulo-giro))
    (slot siguiente (type FUZZY-VALUE angulo-giro))
)

;==========================================================
;--------------------FIN_PLANTILLAS_CAD--------------------
;==========================================================

;==========================================================
;---------------------_PLANTILLAS_CAV_---------------------
;==========================================================
; No es necesario definir nuevas plantillas
;==========================================================
;--------------------FIN_PLANTILLAS_CAV--------------------
;==========================================================

;==========================================================
;---------------------_PLANTILLAS_FA_----------------------
;==========================================================
; No es necesario definir nuevas plantillas
;==========================================================
;--------------------FIN_PLANTILLAS_FA---------------------
;==========================================================

;==========================================================
;--------------Definición de reglas CAD--------------------
;==========================================================

;Nos acercamos a la izquierda -> Hay que ir a la drch
(defrule CAD-izq-1
    (declare (salience 20))
    (instante-analisis (t ?t1) (KLA-dist-izq cerca))
    (instante-analisis (t ?t2) (KLA-dist-izq muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente
    ?gv1))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos acercamos a la izquierda -> Hay que ir a la drch
(defrule CAD-izq-2
    (declare (salience 15))
    (instante-analisis (t ?t1) (KLA-dist-izq normal))
    (instante-analisis (t ?t2) (KLA-dist-izq cerca))
    (test (eq (- ?t2 ?t1) 1))
        ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente
    ?gv1))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Se está muy cerca de la izq -> Hay que ir a la drch
(defrule CAD-izq-3
    (declare (salience 25))
    (instante-analisis (t ?t1) (KLA-dist-izq muy-cerca))
    (instante-analisis (t ?t2) (KLA-dist-izq muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente
    ?gv1))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos desviamos a la drch, hay que cambiar a la izquierda
(defrule CAD-izq-4
    ;(declare (salience 10))
    (instante-analisis (t ?t1) (KLA-dist-izq normal))
    (instante-analisis (t ?t2) (KLA-dist-izq lejos))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la drch y queremos cambiar a la izq
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)

;Nos acercamos a la drch -> Hay que ir a la izq
(defrule CAD-dcha-1
    (declare (salience 20))
    (instante-analisis (t ?t1) (KLA-dist-dcha cerca))
    (instante-analisis (t ?t2) (KLA-dist-dcha muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el siguiente al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos acercamos a la drch -> Hay que ir a la izq
(defrule CAD-dcha-2
    (declare (salience 15))
    (instante-analisis (t ?t1) (KLA-dist-dcha normal))
    (instante-analisis (t ?t2) (KLA-dist-dcha cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el siguiente al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Se está muy cerca de la drch -> Hay que ir a la izq
(defrule CAD-dcha-3
    (declare (salience 25))
    (instante-analisis (t ?t1) (KLA-dist-dcha muy-cerca))
    (instante-analisis (t ?t2) (KLA-dist-dcha muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el siguiente al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?gv1) (siguiente
    ?nuevo-giro))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)
;Nos desviamos a la izquierda, hay que girar a la drch
(defrule CAD-dcha-4
    ;(declare (salience 10))
    (instante-analisis (t ?t1) (KLA-dist-dcha normal))
    (instante-analisis (t ?t2) (KLA-dist-dcha lejos))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-acelerador ?pa) (velocidad ?v) (giro-volante
    ?gv1))
    ;Hay que recuperar el anterior al volante, ya que estamos girando a la izq y queremos cambiar a la drch
    (siguiente-giro-volante (idnivel ?idnivel) (valor ?nuevo-giro) (siguiente ?gv1))
=>
    (retract ?c)
    (printout t "Acción - Girar volante a " ?nuevo-giro crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-giro)
    (assert (controles (id ?id) (pedal-acelerador ?pa) (pedal-freno
    nulo) (velocidad ?v ) (giro-volante ?nuevo-giro) ))
)

;==========================================================
;---------------------Fin de reglas CAD--------------------
;==========================================================

;==========================================================
;-----------------Definición de reglas CAV-----------------
;==========================================================

; Disminuye el nivel del acelerador en caso de sobrepasar la velocidad máxima
(defrule CAV-deceleracion
    (declare (salience 10))
    (instante-analisis (t ?t2)(velocidad-maxima ?vm))
    ?c <- (controles (id ?id)(pedal-acelerador ?pa)(velocidad ?v)(giro-volante ?gv))
    (test (= 0 (mod ?t2 2)))
    (test (> ?v ?vm))
    (siguiente-nivel-pedal (idnivel ?idnivel)(valor ?nuevo-nivel)(siguiente ?pa))
    =>
    (retract ?c)
    (printout t "Acción - Soltar acelerador a " ?nuevo-nivel crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-nivel)
    (assert (controles (id ?id)(pedal-acelerador ?nuevo-nivel)(pedal-freno nulo)(velocidad ?v )(giro-volante ?gv)))
)

;Caso en el que la velocidad actual es menor que la mínima.
;Para acelerar OBLIGATORIAMENTE la distancia con el coche de delante debe ser lejos (distancia de seguridad)
(defrule CAV-aceleracion
    (declare (salience 10))
    ;Para acelerar la distancia debe ser lejos
    (instante-analisis (t ?t2)(sensor-radar lejos)(velocidad-maxima ?vm))
    ?c <- (controles (id ?id)(pedal-acelerador ?pa)(velocidad ?v)(giro-volante ?gv))
    (test (= 0 (mod ?t2 2)))
    (test (< ?v ?vm))
    (siguiente-nivel-pedal (idnivel ?idnivel)(valor ?pa)(siguiente ?nuevo-nivel))
    =>
    (retract ?c)
    (printout t "Acción - Soltar acelerador a " ?nuevo-nivel crlf)
    (plot-fuzzy-value t "*" nil nil ?nuevo-nivel)
    (assert (controles (id ?id)(pedal-acelerador ?nuevo-nivel)(pedal-freno nulo)(velocidad ?v )(giro-volante ?gv)))
)

;==========================================================
;---------------------Fin de reglas CAV--------------------
;==========================================================

;==========================================================
;-----------------Definición de reglas FA------------------
;==========================================================

; Frenado automático FA
(defrule FA1
    (declare (salience 10))
    (instante-analisis (t ?t1) (sensor-radar normal))
    (instante-analisis (t ?t2) (sensor-radar cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-freno ?pa1) (velocidad ?v) (giro-volante
    ?gv))
    =>
    (retract ?c)
    (printout t "Acción - Accionar freno a minimo" crlf)
    (plot-fuzzy-value t "*" nil nil (create-fuzzy-value valor-pedal minimo))
    (assert (controles (id ?id) (pedal-freno minimo) (pedal-acelerador
    nulo) (velocidad ?v ) (giro-volante ?gv) ))
)
(defrule FA2
    (declare (salience 15))
    (instante-analisis (t ?t1) (sensor-radar lejos))
    (instante-analisis (t ?t2) (sensor-radar cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-freno ?pa1) (velocidad ?v) (giro-volante
    ?gv))
    =>
    (retract ?c)
    (printout t "Acción - Accionar freno a moderado" crlf)
    (plot-fuzzy-value t "*" nil nil (create-fuzzy-value valor-pedal moderado))
    (assert (controles (id ?id) (pedal-freno moderado) (pedal-acelerador
    nulo) (velocidad ?v ) (giro-volante ?gv) ))
)
(defrule FA3
    (declare (salience 25))
    (instante-analisis (t ?t1) (sensor-radar cerca))
    (instante-analisis (t ?t2) (sensor-radar muy-cerca))
    (test (eq (- ?t2 ?t1) 1))
    ?c <- (controles (id ?id) (pedal-freno ?pa1) (velocidad ?v) (giro-volante
    ?gv))
    =>
    (retract ?c)
    (printout t "Acción - Accionar freno a fuerte" crlf)
    (plot-fuzzy-value t "*" nil nil (create-fuzzy-value valor-pedal fuerte))
    (assert (controles (id ?id) (pedal-freno fuerte) (pedal-acelerador
    nulo) (velocidad ?v ) (giro-volante ?gv) ))
)

;==========================================================
;---------------------Fin de reglas FA---------------------
;==========================================================

;==========================================================
;  Definición de hechos predefinidos
;==========================================================

; Sequencia de niveles del pedal
(deffacts secuencia-niveles-pedal
    (siguiente-nivel-pedal (idnivel n1) (valor nulo) (siguiente minimo))
    (siguiente-nivel-pedal (idnivel n2) (valor minimo) (siguiente moderado))
    (siguiente-nivel-pedal (idnivel n3) (valor moderado) (siguiente fuerte))
    (siguiente-nivel-pedal (idnivel n4) (valor fuerte) (siguiente afondo))
)

;==========================================================
;  FIN hechos predefinidos
;==========================================================

;==========================================================
;  Definición de hechos CAD
;==========================================================

;El sentido del volante va de drch a izq. Es decir, en la izq está el máximo 
;(por lo que para girar a la drch hay que coger el valor anterior)
; y en la drch el mínimo, por lo que para ir a la izq hay que coger el siguiente
(deffacts secuencia-niveles-volante
    (siguiente-giro-volante (idnivel g1) (valor izq-mucho) (siguiente izq-completo))
    (siguiente-giro-volante (idnivel g2) (valor izq-moderado) (siguiente izq-mucho))
    (siguiente-giro-volante (idnivel g3) (valor izq-medio) (siguiente izq-moderado))
    (siguiente-giro-volante (idnivel g4) (valor izq-poco) (siguiente izq-medio))
    (siguiente-giro-volante (idnivel g5) (valor izq-muy-poco) (siguiente izq-poco))
    (siguiente-giro-volante (idnivel g6) (valor nulo) (siguiente izq-muy-poco))
    (siguiente-giro-volante (idnivel g7) (valor dcha-muy-poco) (siguiente nulo))
    (siguiente-giro-volante (idnivel g8) (valor dcha-poco) (siguiente dcha-muy-poco))
    (siguiente-giro-volante (idnivel g9) (valor dcha-medio) (siguiente dcha-poco))
    (siguiente-giro-volante (idnivel g10) (valor dcha-moderado) (siguiente dcha-medio))
    (siguiente-giro-volante (idnivel g11) (valor dcha-mucho) (siguiente dcha-moderado))
    (siguiente-giro-volante (idnivel g12) (valor dcha-moderado) (siguiente dcha-mucho))
    (siguiente-giro-volante (idnivel g13) (valor dcha-completo) (siguiente dcha-moderado))
)

;==========================================================
;  FIN hechos CAD
;==========================================================

;==========================================================
;  Definición de hechos CAV
;==========================================================
;No es necesario definir nuevos hechos
;==========================================================
;  FIN hechos CAV
;==========================================================

;==========================================================
;  Definición de hechos FA
;==========================================================
;No es necesario definir nuevos hechos
;==========================================================
;  FIN hechos FA
;==========================================================


;===========================================================
; Casos de prueba 
;===========================================================

; Prueba 1
; Verificar que se frena (1)
;(deffacts lista-instantes
;   (instante-analisis (t 1) 
;           (sensor-radar normal) 
;           (KLA-dist-izq normal)
;           (KLA-dist-dcha normal))
;
;   (instante-analisis (t 2) 
;           (sensor-radar cerca) 
;           (KLA-dist-izq normal)
;           (KLA-dist-dcha normal)
;   )
;)

; Prueba 2
; Verificar que se frena (2)
;(deffacts lista-instantes
;    (instante-analisis (t 1) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal))
;
;    (instante-analisis (t 2) 
;            (sensor-radar cerca) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal)
;    )
;)

; Prueba 3
; Verificar que se frena (3)
;(deffacts lista-instantes
;    (instante-analisis (t 1) 
;            (sensor-radar cerca) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal))
;
;    (instante-analisis (t 2) 
;            (sensor-radar muy-cerca) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal)
;    )
;)

; Prueba 4
; Verificar que se acelera
;(deffacts lista-instantes
;    (instante-analisis (t 1)
;            (velocidad-maxima 100)
;            (sensor-radar lejos) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal))
;
;    (instante-analisis (t 2)
;            (velocidad-maxima 100)
;            (sensor-radar muy-lejos) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal)
;    )
;)
;
; Prueba 5
; Verificar que se NO se acelera si la distancia no es de seguridad
;(deffacts lista-instantes
;    (instante-analisis (t 1)
;            (velocidad-maxima 100)
;            (sensor-radar normal) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal))
;
;    (instante-analisis (t 2)
;            (velocidad-maxima 100)
;            (sensor-radar normal) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal)
;    )
;)


; Prueba 6-drch
; Verificar que se gira a la drch cuando se está muy cerca de la izq
;(deffacts lista-instantes
;    (instante-analisis (t 1) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq cerca)
;            (KLA-dist-dcha lejos))
;
;    (instante-analisis (t 2) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq muy-cerca)
;            (KLA-dist-dcha lejos)
;    )
;)

; Prueba 6-izq
; Verificar que se gira a la izq cuando se está muy cerca de la drch
;(deffacts lista-instantes
;    (instante-analisis (t 1) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq lejos)
;            (KLA-dist-dcha cerca))
;
;    (instante-analisis (t 2) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq lejos)
;            (KLA-dist-dcha muy-cerca)
;    )
;)

; Prueba 7-izq
; Verificar que se gira a la izq cuando se está algo descentrado hacia la drch
;(deffacts lista-instantes
;    (instante-analisis (t 1) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal))
;
;    (instante-analisis (t 2) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha cerca)
;    )
;)

; Prueba 7-drch
; Verificar que se gira a la drch cuando se está algo descentrado hacia la izq
;(deffacts lista-instantes
;    (instante-analisis (t 1) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq normal)
;            (KLA-dist-dcha normal))
;
;    (instante-analisis (t 2) 
;            (sensor-radar lejos) 
;            (KLA-dist-izq cerca)
;            (KLA-dist-dcha normal)
;    )
;)
;
(deffacts valores-controles
    (controles (id idCTRL) (pedal-acelerador minimo) (pedal-freno nulo) (giro-volante nulo) (velocidad 80))
)

; Fin Prueba 1

;===============================================================
