\documentclass[10pt,a4paper,titlepage]{article}
\usepackage[utf8x]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[official]{eurosym}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage[table]{xcolor}
\usepackage{geometry}
\geometry{top=2.54cm, left=2.54cm, right=2.54cm, bottom=2.54cm}
\setlength{\parskip}{0.5em}
\usepackage{hyperref}
\usepackage{fancyref}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{wrapfig}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}

\expandafter\def\expandafter\normalsize\expandafter{%
    \normalsize
    \setlength\abovedisplayshortskip{10pt}
    \setlength\belowdisplayshortskip{10pt}
}

\title{ \textbf{ \Huge{Práctica 2: \\ Sistemas Borrosos con FuzzyCLIPS}}}
\author{
        \begin{tabular}{lr}
            \multicolumn{1}{l}{Grupo 83} \\ \cline{1-1} \\
            Daniel Alejandro Rodríguez López & 100316890 \\
            Adrián Borja Pimentel & 100315536 \\
            Mario Montes González & 100318104 \\
            Axel Blanco Cerro & 100315991 \\
        \end{tabular}
}

\begin{document}
\maketitle
\tableofcontents
\newpage

\section{Introducción}
Esta segunda práctica de la asignatura presenta un caso de aplicación de la lógica difusa. Aplicaremos lo aprendido en la teoría para el control de la velocidad, dirección y frenado de un coche automático. Para ello se nos ha proporcionado diferentes valores referidos a los controles del vehículo (distancia lateral, distancia frontal, pedales, etc.). La implementación se realizará con el software de sistema experto FuzzyCLIPS.

En los siguientes apartados describimos las reglas difusas implementadas sobre las variables que son requeridas en el enunciado y las pruebas realizadas para comprobar el correcto funcionamiento del sistema en conjunto. Finalizamos con un apartado de conclusiones e impresiones personales sobre la práctica.

\section{Control adaptativo de velocidad de crucero (CAV)}
El sistema de Control Adaptativo de Velocidad de Crucero es un sistema que permite a nuestro coche autónomo recalcular la velocidad de crucero en base al estado del vehículo que se recoge como reglas difusas. Es suficiente con dos reglas modelar el comportamiento apropiado (aceleración y freno):

La primera regla define el caso de deceleración, que se produce cuando la velocidad del vehículo supera la velocidad máxima de la vía en el instante actual. En ese caso se `levanta el pie" del pedal del acelerador, haciendo que pase al estado inmediatamente anterior (por ejemplo, de moderado a mínimo). Únicamente debe, en este caso, preocuparnos las velocidades ya que de las distancias se ocupará el Sistema de Frenado.

La segunda regla contempla el caso contrario, la aceleración. Esta se produce cuando la velocidad del coche autónomo es menor que la velocidad máxima de la vía en el instante actual, en cuyo caso se aumenta el valor del pedal del acelerador al siguiente estado (por ejemplo, de moderado a fuerte). En este caso si se debe tener en cuenta también la distancia, ya que la distancia de seguridad prima sobre la velocidad. De esta manera, hemos añadido como precondición de la regla que la distancia sea lejos (la definida como distancia de seguridad), que debe cumplirse junto con lo dicho anteriormente para que se pueda activar la regla.

Por último, estas reglas operan solamente sobre los instantes pares (en cada par de instantes que se comparan, sobre el segundo) ya que contemplan únicamente el estado actual, no son dependientes de cual fuese la configuración en el estado anterior. Además, en estas reglas se marca en la parte izquierda que el estado del control del pedal de freno sea nulo, ya que en el enunciado se especifica que hay una incompatibilidad entre estos dos pedales (si está uno activado, el otro no debe estarlo).

\section{Sistema de frenado automático (FA)}
El sistema de Frenado Automático de nuestro coche autónomo mediante lógica difusa tiene una reglas simples que aparecen en el enunciado. Las tres primeras se corresponden con reglas directas en FuzzyCLIPS y la cuarta hace referencia a una modificación de todas las anteriores.

La regla \texttt{FA1} comprueba la variación del radar entre dos instantes consecutivos, y si esta varía de normal a cerca acciona el pedal de freno de forma mínima. El pedal de acelerador se fija a nulo.

La regla \texttt{FA2} comprueba la variación del radar entre dos instantes consecutivos, y si esta varía de lejos a cerca acciona el pedal de freno de forma moderada. El pedal de acelerador se fija a nulo.

La regla \texttt{FA3} comprueba la variación del radar entre dos instantes consecutivos, y si esta varía de cerca a muy cerca acciona el pedal de freno de forma fuerte. El pedal de acelerador se fija a nulo.

En los tres casos, el pedal del acelerador se selecciona como valor nulo ya que esto es un requisito del enunciado. En todas las reglas se elimina el valor anterior de los controles y se sustituye por el nuevo creado, además se gráfica el valor difuso obtenido.

\section{Control automático de la dirección (CAD)}
El sistema de Control Automático de Dirección de nuestro coche autónomo mediante lógica difusa ha requerido la definición de nuevas plantillas para los siguientes giros de volante, ya que para girar en un sentido u otro es necesario conocer el estado anterior o siguiente, nuevas definiciones de hechos y, junto con esto, la definición de nuevas reglas para poder realizar los cambios en el volante.

Se utilizan 8 reglas en total, 4 para corregir los desvíos hacia la izquierda y otras 4 para cuando los desvíos se dan hacia la derecha.

Cada una de estas reglas hace un calculo de la distancia longitudinal en dos instantes consecutivos y en base a la distancia y la dirección varía el ángulo de giro del volante. Las situaciones contempladas en las 4 reglas de cada dirección son:

\begin{itemize}
    \item \texttt{Situación 1}: La distancia longitudinal varía entre cerca y muy cerca. 
    \item \texttt{Situación 2}: La distancia longitudinal varía entre normal y cerca. 
    \item \texttt{Situación 3}: La distancia longitudinal varía entre valores pertenecientes a muy cerca, por lo que el giro debería ser lo más alto posible. 
    \item \texttt{Situación 4}: La distancia longitudinal varía entre normal y lejos, en cuyo caso el giro debe corregir la dirección para aproximarse a la línea.
\end{itemize}

En todas estas situaciones, el giro del volante a realizar debe ser en sentido contrario al estado actual. Dado que en estas reglas sólo se opera sobre el ángulo de giro del volante, el resto de variables se mantienen.


\section{Pruebas}
Para la realización de pruebas se han establecido diferentes escenarios con el objetivo de disparar (o no) una regla en concreto. Estas se pueden encontrar al final del fichero .clp entregado, con el identificador de la prueba y el objetivo que se intenta alcanzar.

\begin{itemize}
    \item \texttt{Prueba 1}: El objetivo de la prueba es verificar que el coche frena cuando pasa de una distancia Normal a Cerca con el coche de delante.
    \item \texttt{Prueba 2}: El objetivo de la prueba es verificar que el coche frena cuando pasa de una distancia Lejos a Cerca con el coche de delante.
    \item \texttt{Prueba 3}: El objetivo de la prueba es verificar que el coche frena cuando pasas de una distancia Cerca a Muy-Cerca con el coche de delante.
    \item \texttt{Prueba 4}: El objetivo de la prueba es verificar que el coche acelera cuando se encuentra a distancia de seguridad y su velocidad no es todavía la velocidad máximo.
    \item \texttt{Prueba 5}: El objetivo de la prueba es verificar que el coche no acelera cuando se encuentra a una distancia que no sea de seguridad.
    \item \texttt{Prueba 6-drch}: El objetivo de la prueba es verificar que el coche gira a la derecha cuando pasa de Cerca a Muy-cerca en la distancia lateral izquierda.
    \item \texttt{Prueba 6-izq}: El objetivo de la prueba es verificar que el coche gira a la izquierda cuando pasa de Cerca a Muy-cerca en la distancia lateral derecha.
    \item \texttt{Prueba 7-izq}: El objetivo de la prueba es verificar que el coche gira a la izquierda cuando pasa de Normal a Cerca en la distancia lateral derecha.
    \item \texttt{Prueba 7-drch}: El objetivo de la prueba es verificar que el coche gira a la derecha cuando pasa de Normal a Cerca en la distancia lateral izquierda.
\end{itemize}

Todas las pruebas concluyeron exitosamente. Sin embargo, en algunos casos, la acción realizada era correcta pero la nueva instancia de \texttt{controles} no se creaba correctamente y se desconoce el motivo ya que los valores con los que se debe crear esa nueva instancia eran correctos.

\section{Conclusiones y reflexiones sobre la práctica}
Gracias a esta práctica hemos podido observas cómo funcionan realmente los sistemas difusos en la realidad, no solo de forma teórica, observando las gráficas generadas por FuzzyCLIPS. Además, hemos empleado el sistema experto CLIPS comprobando la ejecución del motor de inferencia paso a paso, qué reglas y cuándo se disparan. Sin embargo, con una sóla clase de CLIPS (lenguaje que se ha visto por primera vez en todo el grado), y quizás es decir mucho ya que no fueron 100 minutos de explicar la sintaxis y funcionamiento de CLIPS, es complicado enfrentarse a un problema de lógica difusa, añadir lógica nueva y aún más complicado testearlo para saber si funciona bien. 

En definitiva, aunque todo parezcan cosas malas, nos ha servido para asentar los conocimientos vistos en clase y ver de forma superficial la complejidad de un sistema basado en lógica difusa.


\end{document}
