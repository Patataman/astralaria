\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{2}{section.1}
\contentsline {section}{\numberline {2}Miner\IeC {\'\i }a de texto de forma manual}{2}{section.2}
\contentsline {section}{\numberline {3}Miner\IeC {\'\i }a de texto RapidMiner}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}C\IeC {\'a}lculo de similitud}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Generaci\IeC {\'o}n de grupos}{4}{subsection.3.2}
\contentsline {section}{\numberline {4}Consideraciones adicionales}{5}{section.4}
\contentsline {section}{\numberline {5}Conclusiones y reflexiones sobre la pr\IeC {\'a}ctica}{6}{section.5}
