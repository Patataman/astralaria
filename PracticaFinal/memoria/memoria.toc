\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}El videojuego}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Estado del arte}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Investigaci\IeC {\'o}n}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Aplicaciones similares}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Telegram}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Predicci\IeC {\'o}n}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Contexto de la serie temporal y procesado de datos}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Multilayer Perceptron}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Long Sort-Term Memory}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Conclusiones generales}{6}{subsection.3.4}
\contentsline {section}{\numberline {4}Interfaz}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Bot en Telegram}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Natural Language Processing}{8}{subsection.4.2}
\contentsline {section}{\numberline {5}Conclusiones personales}{9}{section.5}
\contentsline {section}{\numberline {6}Referencias}{9}{section.6}
\contentsline {paragraph}{Algunos trabajos de investigaci\IeC {\'o}n}{9}{section*.11}
\contentsline {paragraph}{Sobre Guild Wars 2}{10}{section*.12}
\contentsline {paragraph}{API de Guild Wars 2}{10}{section*.13}
\contentsline {paragraph}{TensorFlow}{10}{section*.14}
\contentsline {paragraph}{Conceptos generales sobre LSTM}{10}{section*.15}
