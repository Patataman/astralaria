import tensorflow as tf
import numpy as np
from process import Process
from sklearn.metrics import mean_squared_error
import time

n_iterations = 100 # numer of iterations
input_size = 12  # process instances * 2
hidden_neurons = 10 # numer of hidden neurons
lr = 0.01 # learning rate
obj_type = 'sell'
obj_id = '24344'

# Initializes weights
def init_weights(shape):
    return tf.Variable(tf.random_normal(shape, stddev=0.01))

# Initializes a model with sigmoidal activation function
def model(X, w_h, w_o):
    h = tf.nn.sigmoid(tf.matmul(X, w_h))
    return tf.matmul(h, w_o)

#Training variables
trX = []
trY = []
#Test variables
teX = []
teY = []

# get data
proc = Process(6)
proc.setup_data()

for instance in proc.get_data(obj_type, 1, obj_id)['train']:
    trX.append(instance['inputs'])
    trY.append(instance['outputs'])

trX = np.array(trX)
trY = np.array(trY)

for instance in proc.get_data(obj_type, 1, obj_id)['test']:
    teX.append(instance['inputs'])
    teY.append(instance['outputs'])

teX = np.array(teX)
teY = np.array(teY)


X = tf.placeholder("float", [None, input_size]) # network input (quantity and price)
Y = tf.placeholder("float", [None, 1]) # output

w_h = init_weights([input_size, hidden_neurons])  # first layer weights [input_neurons, hidden_neurons]
w_o = init_weights([hidden_neurons, 1])  # second layer weights [hidden_neurons, output_neurons]

py_x = model(X, w_h, w_o)

cost = tf.reduce_mean(tf.contrib.losses.mean_squared_error(predictions=py_x, labels=Y)) # cost
#cost = tf.reduce_mean(tf.nn.l2_loss(py_x)) # cost
train_op = tf.train.GradientDescentOptimizer(lr).minimize(cost) # optimizer
predict_op = py_x

# Launch session
with tf.Session() as sess:
    # you need to initialize all variables
    tf.global_variables_initializer().run()
    # buy_iterations_
    file_name = "%s_%s_%s_%s_%s_%s.txt" % (time.strftime("%Y%m%d_%H-%M-%S"), obj_type, n_iterations, hidden_neurons, lr, obj_id)
    f = open('experiments/%s' % file_name, 'a')
    f.write('iteration,train_error,test_error')


    #Ciclos de entrenamiento
    for i in range(n_iterations):
        sess.run(train_op, feed_dict={X: trX, Y: trY})
        result = "%s, %s, %s\n" % (i, mean_squared_error(trY, sess.run(predict_op, feed_dict={X: trX})),
                                   mean_squared_error(teY, sess.run(predict_op, feed_dict={X: teX})))
        f.write(result)
        print(result)

    #prueba = [[10,15,10,10,10,15,10,14,15,12,14,12]]
    #predicc=sess.run(predict_op, feed_dict={X: prueba})
    #print(predicc)
    #print(proc.denormalize_value(predicc, obj_id, obj_type))
