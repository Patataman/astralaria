import tensorflow as tf
import numpy as np
from process import Process
from sklearn.metrics import mean_squared_error

n_iterations = 100 # numer of iterations
input_size = 10  # process instances * 2
hidden_neurons = 10 # numer of hidden neurons
lr = 0.01 # learning rate
id = '24344'

# Initializes weights
def init_weights(shape):
    return tf.Variable(tf.random_normal(shape, stddev=0.01))

# Initializes a model with sigmoidal activation function
def model(X, w_h, w_o):
    h = tf.nn.sigmoid(tf.matmul(X, w_h))
    return tf.matmul(h, w_o)

#Training variables
trX = []
trY = []
#Test variables
teX = []
teY = []

# get data
proc = Process()
proc.setup_data()

for instance in proc.get_data('sell', 1, id)['train']:
    trX.append(instance['inputs'])
    trY.append(instance['outputs'])

trX = np.array(trX)
trY = np.array(trY)

for instance in proc.get_data('sell', 1, id)['test']:
    teX.append(instance['inputs'])
    teY.append(instance['outputs'])

teX = np.array(teX)
teY = np.array(teY)


X = tf.placeholder("float", [None, input_size]) # network input (quantity and price)
Y = tf.placeholder("float", [None, 1]) # output

w_h = init_weights([input_size, hidden_neurons])  # first layer weights [input_neurons, hidden_neurons]
w_o = init_weights([hidden_neurons, 1])  # second layer weights [hidden_neurons, output_neurons]

py_x = model(X, w_h, w_o)

cost = tf.reduce_mean(tf.contrib.losses.mean_squared_error(predictions=py_x, labels=Y)) # cost
#cost = tf.reduce_mean(tf.nn.l2_loss(py_x)) # cost
train_op = tf.train.GradientDescentOptimizer(lr).minimize(cost) # optimizer
predict_op = py_x

# Launch session
with tf.Session() as sess:
    # you need to initialize all variables
    tf.global_variables_initializer().run()

    #Ciclos de entrenamiento
    for i in range(n_iterations):
        sess.run(train_op, feed_dict={X: trX, Y: trY})
        result = "%s, %s" % (i, mean_squared_error(teY, sess.run(predict_op, feed_dict={X: teX})))
        # print(sess.run(predict_op, feed_dict={X: teX}))
        print(result)
